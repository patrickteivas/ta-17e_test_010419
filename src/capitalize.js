var stringCapitalize = require("string-capitalize");

/**
 * get capitalized string
 *
 * @returns {string}
 */
module.exports = function capitalize(text) {
	if(typeof text !== "string") {
		throw "bad input";
	}
	return stringCapitalize(text);
};
